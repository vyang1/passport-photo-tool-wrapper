![Build Status](https://gitlab.com/pages/harp/badges/master/build.svg)

---

[Harp] website using GitLab Pages for hosting the Passport Photo Tool

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

Wrapper for Department of State's free photo tool: 
https://travel.state.gov/content/travel/en/us-visas/visa-information-resources/photos/digital-image-requirements.html

h/t https://medium.com/@marudhuri/simple-workaround-to-get-the-passport-photo-tool-travel-state-gov-working-on-chrome-mac-8435e9ba9f08